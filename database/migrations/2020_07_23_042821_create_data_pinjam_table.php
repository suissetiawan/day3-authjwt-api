<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataPinjamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_pinjam', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('tgl_pinjam');
            $table->dateTime('tgl_deadline');
            $table->dateTime('tgl_kembali')->nullable();
            $table->boolean('status_ontime')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
            $table->unsignedBigInteger('databuku_id');
            $table->foreign('databuku_id')
                  ->references('id')
                  ->on('data_buku')
                  ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_pinjam');
    }
}
