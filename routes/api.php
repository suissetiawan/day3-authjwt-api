<?php
 
Route::namespace('Auth')->group(function(){
	Route::post('register','RegisterController');
	Route::post('login','LoginController');
	Route::post('logout','LogoutController');
	
});

//--------akses all User-------------
Route::middleware('auth:api')->group(function(){
	Route::get('user','UserController');
	Route::get('detail-user/{id}','UserController@show');
	Route::get('biodata-user/{id}','BiodataController@show');
	Route::get('detail-peminjam/{id}','DatapinjamController@show');
	Route::get('daftar-buku','DatabukuController@index');
	Route::get('detail-buku/{id}','DatabukuController@show');
});


//---------Hanya boleh diakses admin-----------
Route::middleware(['auth:api','role'])->group(function(){
	Route::get('daftar-user','UserController@index');
	Route::post('tambah-user','UserController@store');
	Route::delete('hapus-user/{id}','UserController@destroy');

	Route::post('tambah-biodata','BiodataController@store');
	Route::patch('edit-biodata/{id}','BiodataController@update');

	Route::post('tambah-buku','DatabukuController@store');
	Route::patch('edit-buku/{id}','DatabukuController@update');
	Route::delete('hapus-buku/{id}','DatabukuController@destroy');	

	Route::get('peminjam','DatapinjamController@index');
	Route::post('tambah-peminjam','DatapinjamController@store');
	Route::delete('hapus-peminjam/{id}','DatapinjamController@destroy');	
	Route::patch('edit-peminjam/{id}','DatapinjamController@update');
});


