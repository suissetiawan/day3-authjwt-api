<?php

namespace App\Http\Controllers;

use App\Models\Datapinjam;
use App\Models\User;
use App\Models\Databuku;
use App\Http\Resources\PeminjamResource;
use Illuminate\Http\Request;

class DatapinjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjam = Datapinjam::get();
        return PeminjamResource::collection($pinjam);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Datapinjam::create($this->peminjamStore());
        return "peminjam berhasil ditambahkan";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Datapinjam  $datapinjam
     * @return \Illuminate\Http\Response
     */
    public function show($datapinjam)
    {
        $data = Datapinjam::find($datapinjam);
        return new PeminjamResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Datapinjam  $datapinjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $datapinjam)
    {
        // $data = ;
        // dd($request->all());
        Datapinjam::find($datapinjam)->update($this->peminjamStore());
        return new PeminjamResource(Datapinjam::find($datapinjam));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Datapinjam  $datapinjam
     * @return \Illuminate\Http\Response
     */
    public function destroy($datapinjam)
    {
        Datapinjam::find($datapinjam)->delete();
        return Response()->json('The peminjaman dihapus',200);
    }

    public function peminjamStore(){
        return [
            'tgl_pinjam' => request('tgl_pinjam'),
            'tgl_deadline' => request('tgl_deadline'),
            'user_id' => Request('user_id'),
            'databuku_id' => Request('databuku_id'),
            'tgl_kembali' => Request('tgl_kembali'),
            'status_ontime' => Request('status_ontime'),
        ];
    }
}
