<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //dd(Auth::user()->isRole());
        return $request->user()->email;
    }

    public function index()
    {
        $user = User::get();
        return UserResource::collection($user);
    }

    public function show($id)
    {
        $user = User::find($id);
        return new UserResource($user);
    }

    public function store(Request $request)
    {
        User::create($this->userStore());
        return "user berhasil ditambahkan";
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return Response()->json('user telah dihapus',200);
    }

    public function userStore(){
        return [
            'username' => request('username'),
            'email' => request('email'),
            'role_id' => request('role_id'),
            'biodata_id' => request('biodata_id'),
            'password' => bcrypt(request('password')),
        ];
    }
}
