<?php

namespace App\Http\Controllers;

use App\Models\Biodata;
use Illuminate\Http\Request;
use App\Http\Resources\BiodataResource;

class BiodataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Biodata::create($this->biodataStore());
        return "Biodata berhasil ditambahkan";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //dd($biodata);
        $bio = Biodata::find($id);
        return new BiodataResource($bio);
    }

    public function update(Request $request, $id)
    {

        Biodata::find($id)->update($this->biodataStore());
        return new BiodataResource(Biodata::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Biodata  $biodata
     * @return \Illuminate\Http\Response
     */
    public function destroy(Biodata $biodata)
    {
        //
    }

    public function biodataStore(){
        return [
            'nama' => request('nama'),
            'nim' => request('nim'),
            'jurusan' => request('jurusan'),
            'fakultas' => request('fakultas'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
        ];
    }
}
