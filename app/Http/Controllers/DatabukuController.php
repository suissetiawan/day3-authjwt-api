<?php

namespace App\Http\Controllers;

use App\Models\Databuku;
use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Resources\DatabukuResource;

class DatabukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Databuku::get();
        return DatabukuResource::collection($buku);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Databuku::create($this->bukuStore());
        return "buku berhasil ditambahkan";
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Databuku  $databuku
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Databuku::find($id);
        return new DatabukuResource($buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Databuku  $databuku
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        Databuku::find($id)->update($this->bukuStore());
        return new DatabukuResource(Databuku::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Databuku  $databuku
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Databuku::find($id)->delete();
        return Response()->json('buku berhasil dihapus',200);
    }

    public function bukuStore(){
        return [
            'kode' => Request('kode'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => Request('tahun_terbit'),
        ];
    }
}
