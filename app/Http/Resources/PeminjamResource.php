<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PeminjamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return /*parent::toArray($request)*/[
            'peminjam' => $this->user->biodata->nama,
            'buku_pinjaman' => $this->user->buku,
            'tgl_peminjaman' => $this->tgl_pinjam,
            'tgl_harus_kembali' => $this->tgl_deadline,
            'status' => $this->status_ontime,
        ];
    }
}
