<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return /*parent::toArray($request)*/[
            'nama' => $this->biodata->nama,
            'nim' => $this->biodata->nim,
            'jurusan' => $this->biodata->jurusan,
            'fakultas' => $this->biodata->fakultas,
            'role' => $this->role->role,
            'email' => $this->email,
        ];
    }
}
