<?php

namespace App\Models;

use App\Models\User;
use App\Models\Databuku;
use Illuminate\Database\Eloquent\Model;

class Datapinjam extends Model
{
    protected $table = 'data_pinjam';
    protected $fillable = ['tgl_pinjam', 'tgl_deadline', 'tgl_kembali', 'user_id','databuku_id','status_ontime'];

    public function user(){
        return $this->belongsTo(User::class);
    }



    
}
