<?php

namespace App\Models;

use App\Models\Role;
use App\Models\Biodata;
use App\Models\Datapinjam;
use App\Models\Databuku;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id','biodata_id', 'email', 'password','username',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function isRole(){
        
        if ($this->role_id == 1 ){
            return true;
        }else{
         return false;
        }
    }

    public function biodata(){

        return $this->BelongsTo(Biodata::class);
    }

    public function role(){

        return $this->BelongsTo(Role::class);
    }

    public function pinjam(){

        return $this->HasMany(Datapinjam::class);
    }

    public function buku(){
        return $this->belongsToMany('App\Models\Databuku','data_pinjam','user_id','databuku_id');
    }
}
