<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Biodata extends Model
{
    protected $table = 'biodata';
    protected $fillable = ['nama', 'nim', 'fakultas', 'jurusan','no_hp','no_wa'];

}
