<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Datapinjam;
use App\Models\User;

class Databuku extends Model
{
    //
    protected $table = 'data_buku';
    protected $fillable = ['kode', 'judul','pengarang','tahun_terbit'];

    public function User(){
        return $this->belongsToMany('App\Models\User','data_pinjam','user_id','databuku_id');
    }

}
